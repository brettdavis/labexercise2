// Kathy Xiong
// Lab Exercise 2

#include <iostream>
#include <conio.h>

using namespace std;

enum Suit
{
	Spade, Heart, Diamond, Club
};

enum Rank
{
	Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

struct Card
{
	Rank rank;
	Suit suit;
};

//Brett Davis
//Part 2 edited on: 9-11-2019
void PrintCard(Card card)
{
	switch (card.rank)
	{
	case 14: cout << "The Ace "; break;
	case 13: cout << "The King "; break;
	case 12: cout << "The Queen "; break;
	case 11: cout << "The Jack "; break;
	case 10: cout << "The Ten "; break;
	case 9: cout << "The Nine "; break;
	case 8: cout << "The Eight "; break;
	case 7: cout << "The Seven "; break;
	case 6: cout << "The Six "; break;
	case 5: cout << "The Five "; break;
	case 4: cout << "The Four "; break;
	case 3: cout << "The Three "; break;
	case 2: cout << "The Two "; break;
	default: cout << "error";
	}

	switch (card.suit)
	{
	case 3: cout << "of Clubs"; break;
	case 2: cout << "of Diamonds"; break;
	case 1: cout << "of Hearts"; break;
	case 0: cout << "of Spades"; break;
	default: cout << "error";
	}
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)  return card1;
	else if (card1.rank < card2.rank) return card2;
	else if (card1.suit > card2.suit) return card1;
	else return card2;
}

int main()
{
	/*Testing if my functions worked correctly
	Card card1;
	card1.rank = Ten;
	card1.suit = Club;
	Card card2;
	card2.rank = Ten;
	card2.suit = Spade;
	PrintCard(HighCard(card1, card2));
	*/
	_getch();
	return 0;
}
